const crypto = require('crypto'),
fs = require('fs');

const utils = {
  hash_items: function(cb){
    const arr = ['admin','extensions','blog','mobile'];

    let obj = {},
    item;

    for (let i = 0; i < arr.length; i++) {
      item = utils.sha3(Buffer.from(fs.readFileSync('./db/db_'+ arr[i] +'.json', 'utf8'), 'binary'),{
        len: 512,
        digest: 'base64'
      });
      if(item !== null){
        obj['db_'+ arr[i]] = {
          hash: item,
          date: Date.now()
        }
      } else {
        return cb('db_'+ arr[i] +' hash error');
        break;
      }
    }

    fs.writeFile('./db/index.json', JSON.stringify(obj,0,2), function(err){
      if(err){return cb('unable to update db hash')}
      cb(false, 'item hash update success')
    })
  },
  sha3: function(data, obj, cb){
    try {
      return crypto.createHash('sha3-'+ obj.len).update(data).digest(obj.digest);
    } catch (err) {
      return null;
    }
  }
}

module.exports = utils;
