const utils = require('./utils'),
cl = console.log,
ce = console.error

utils.hash_items(function(err, res){
  if(err){return ce(err)}
  return cl(res)
})
